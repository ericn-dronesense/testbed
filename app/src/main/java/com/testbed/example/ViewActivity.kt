package com.testbed.example

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import com.testbed.example.databinding.ActivityMainBinding

class ViewActivity : AppCompatActivity(R.layout.activity_main) {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        with(ActivityMainBinding.inflate(layoutInflater)) {
            setContentView(root)
            root.addView(getView(this@ViewActivity, root))
            root.addView(getView(MainActivity.instance, root))
        }
    }

    private fun getView(context: Context?, parent: ViewGroup): View {
        return LayoutInflater.from(context).inflate(R.layout.partial_text, parent, false)
    }
}
